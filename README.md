# morgan
*Che succede?*

Questo è un repository inusuale: qui si trovano le idee e le proposte per avviare nuove attività in associazione. In questo modo si può tener traccia del tempo dedicato e dell'avanzamento dei progetti.

Uno dei motivi per cui esiste questo "issue tracker" è il fatto che può rimanere una traccia di cosa facciamo, cosa c'è da fare, e come si fanno le cose, e delle discussioni che abbiamo durante il processo decisionale.

Questo è importante, perché, anche se i problemi non sono tanti, la natura volontaria della nostra associazione fa sì che, spesso, i tempi di applicazione siano molto lunghi, per cui capita di dimenticarsi cosa c'è da fare o come, e questo repository è qui per ricordarsi questo genere di cose.

Se vuoi prendere parte alla discussione
* chiedi un account per accedere a Gitea
* apri una issue, oppure commentane una preesistente

## Note
* Ogni idea deve avere almeno un responsabile attività che ne curi/coordini lo svolgimento;
* Abusare delle etichette per definirne lo stadio di avanzamento (e.g. Idea, In preparazione, ...);
* Una volta che un'idea si concretizza, può essere riassunta sul wiki nella pagina dei [Progetti](https://golem.linux.it/wiki/Progetti).

## Credits
Da un'idea di [Italian Linux Society](https://gitlab.com/ItalianLinuxSociety/brainstorm), nome proposto da [giomba](https://git.golem.linux.it/giomba), appunto per sapere "che cosa succede", visto che spesso non ce lo ricordiamo nemmeno noi (in buona e in cattiva fede :angel: :angry: ).

![Che succede?](morgan.jpeg)
